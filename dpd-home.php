<?php 
class WC_DPD_Home extends WC_Shipping_Method {

	public function __construct( $instance_id = 0 ) {
		$this->id                    = 'dpd-home';
		$this->instance_id           = absint( $instance_id );
		$this->method_title          = __( 'DPD HOME' );
		$this->method_description    = __( 'The product DPD Home offers a responsible service of high-quality delivery of
the consignments to private persons. It provides the transfer of the parcel
from one of your approved pickup addresses to a recipient home address. It
provides these benefits:
- Recipient is notified of delivery one day in advance by an SMS or email.
- Recipient has a possibility to change the day and/or address of delivery.
- Recipient is notified about the delivery time window with SMS/email sent in the day of delivery.
- Recipient has a possibility to pay for the COD with a bank card.
- If a courier doesn’t reach a recipient at home, he shall leave notification in his letterbox.
This product is specified by the product code 9.' );
    $this->supports              = array(
			'shipping-zones',
			'instance-settings',
		);
		$this->instance_form_fields = array(
			'enabled' => array(
				'title' 		=> __( 'Enable/Disable' ),
				'type' 			=> 'checkbox',
				'label' 		=> __( 'Enable this shipping method' ),
				'default' 		=> 'yes',
			),
			'title' => array(
				'title' 		=> __( 'Method Title' ),
				'type' 			=> 'text',
				'description' 	=> __( 'This controls the title which the user sees during checkout.' ),
				'default'		=> __( 'DPD Home' ),
				'desc_tip'		=> true
			)
		);
		
		
		$this->enabled              = $this->get_option( 'enabled' );
		$this->title                = $this->get_option( 'title' );

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}
    public function calculate_shipping( $package = array() ) {
	$this->add_rate( array(
		'id'    => $this->id . $this->instance_id,
		'label' => $this->title,
		'cost'  => 0,
	) );
}
}

?>