<?php 
class WC_DPD_Express extends WC_Shipping_Method {

	public function __construct( $instance_id = 0 ) {
		$this->id                    = 'dpd-express';
		$this->instance_id           = absint( $instance_id );
		$this->method_title          = __( 'DPD EXPRESS' );
		$this->method_description    = __( ' PODLE SPECIFIKACE NENI JASNE JAKY PRODUCT EXPRESS CHTEJI' );
    $this->supports              = array(
			'shipping-zones',
			'instance-settings',
		);
		$this->instance_form_fields = array(
			'enabled' => array(
				'title' 		=> __( 'Enable/Disable' ),
				'type' 			=> 'checkbox',
				'label' 		=> __( 'Enable this shipping method' ),
				'default' 		=> 'yes',
			),
			'title' => array(
				'title' 		=> __( 'Method Title' ),
				'type' 			=> 'text',
				'description' 	=> __( 'This controls the title which the user sees during checkout.' ),
				'default'		=> __( 'DPD Express' ),
				'desc_tip'		=> true
			)
		);
		
		
		$this->enabled              = $this->get_option( 'enabled' );
		$this->title                = $this->get_option( 'title' );

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}
    public function calculate_shipping( $package = array() ) {
	$this->add_rate( array(
		'id'    => $this->id . $this->instance_id,
		'label' => $this->title,
		'cost'  => 0,
	) );
}
}

?>