<?php 
class WC_DPD_Classic extends WC_Shipping_Method {


	public function __construct( $instance_id = 0 ) {
		$this->id                    = 'dpd-classic';
		$this->instance_id           = absint( $instance_id );
		$this->method_title          = __( 'DPD CLASSIC' );
		$this->method_description    = __( 'Our Classic parcel service is the most economical solution for domestic shipping.
It provides the transfer of the parcel from one of your approved pickup
addresses to a recipient business address. This service is appropriate for business
customers. It provides these benefits:
- Standard delivery time just next day.
- Includes insurance of up to 2 500 € per parcel, Higher insurance available.
- Three delivery attempts,
- Online proof of delivery, including recipients confirmation of receipt (P.O.D.) on the Internet
This product is specified by the product code 1.' );
    $this->supports              = array(
			'shipping-zones',
			'instance-settings',
		);
		$this->instance_form_fields = array(
			'enabled' => array(
				'title' 		=> __( 'Enable/Disable' ),
				'type' 			=> 'checkbox',
				'label' 		=> __( 'Enable this shipping method' ),
				'default' 		=> 'yes',
			),
			'title' => array(
				'title' 		=> __( 'Method Title' ),
				'type' 			=> 'text',
				'description' 	=> __( 'This controls the title which the user sees during checkout.' ),
				'default'		=> __( 'DPD Classic' ),
				'desc_tip'		=> true
			)
		);
		
		
		$this->enabled              = $this->get_option( 'enabled' );
		$this->title                = $this->get_option( 'title' );

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}
    public function calculate_shipping( $package = array() ) {
	$this->add_rate( array(
		'id'    => $this->id . $this->instance_id,
		'label' => $this->title,
		'cost'  => 0,
	) );
}
}

?>